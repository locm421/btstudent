package com.example.BtStudents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtStudentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BtStudentsApplication.class, args);
	}

}
