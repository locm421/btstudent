package com.example.BtStudents.controller;

import com.example.BtStudents.dto.Student;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/baitap")
public class StudentController {
    List<Student> listStudentsParam = new ArrayList<>();
    List<Student> listStudentsPathVariable = new ArrayList<>();
    List<Student> listStudentsReqBody = new ArrayList<>();
    @GetMapping("/students/q")
    public List<Student> addStudentParam(@RequestParam String name, @RequestParam int age)
    {
        Student student = new Student();
        student.setName(name);
        student.setAge(age);
        listStudentsParam.add(student);
        return listStudentsParam;
    }
    @GetMapping("/students/{name}/{age}")
    public List<Student> addStudentPathVariable(@PathVariable String name, @PathVariable int age)
    {
        Student student = new Student();
        student.setName(name);
        student.setAge(age);
        listStudentsPathVariable.add(student);
        return listStudentsPathVariable;
    }
    @PostMapping("/students")
    public List<Student> addStudentReqBody(@RequestBody Student student){
        listStudentsReqBody.add(student);
        return listStudentsReqBody;
    }
}
